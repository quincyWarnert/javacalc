/**
 * Educom
 */
package nu.educom.calculateChange;

import java.util.Map;
import java.util.TreeMap;

import javax.management.InvalidApplicationException;

/**
 * Main class for calculate change
 */
public class Main {

	/**
	 * main entry point for the program
	 * 
	 * @throws InvalidApplicationException when an error occurs
	 */
	public static void main(String[] args) throws InvalidApplicationException {
		
		/* a Map is used as an associative array to hold the initial data */
		Map<Money, Integer> initialContent = new TreeMap<Money, Integer>(new ReverseComparer());
		initialContent.put( new Money(50   ),  0 ); // no bills of 50 euro
		initialContent.put( new Money(20   ),  1 ); //  1 bill  of 20 euro
		initialContent.put( new Money(10   ),  0 ); // no bills of 10 euro
		initialContent.put( new Money( 5   ),  1 ); //  1 bill  of  5 euro
		initialContent.put( new Money( 2   ),  1 ); //  1 coin  of  2 euro
		initialContent.put( new Money( 1   ),  4 ); //  4 coins of  1 euro
		initialContent.put( new Money( 0.5 ),  2 ); //  2 coins of 50 cent
		initialContent.put( new Money( 0.2 ), 10 ); // 10 coins of 20 cent
		initialContent.put( new Money( 0.1 ),  3 ); //  3 coins of 10 cent
		initialContent.put( new Money( 0.05),  7 ); //  7 coins of  5 cent
		initialContent.put( new Money( 0.02),  1 ); //  1 coin  of  2 cent
		initialContent.put( new Money( 0.01),  5 ); //  5 coins of  1 cent		

		CashRegister register = new CashRegister(initialContent);
		
		Map<Money, Integer> result = register.makeChange(new Money(4.51), new Money(10));
        showAmount("MakeChange for 4,51 from 10 euro", result); // 5, .20, .20, .05, .02, .01, .01

        result = register.makeChange(new Money(1.30), new Money(5));
        showAmount("MakeChange for 1,30 from 5 euro", result); // 2, 1, .5, .2

        result = register.makeChange(new Money(0.97), new Money(20));
        showAmount("MakeChange for 0,97 from 20 euro", result); // 10, 5, 1, 1, 1, .5, .2, .2, .1, .01, .01, .01

        Money profit = register.processEndOfDay();

        showProfit(profit);

        if (!profit.equals(new Money(6.78))) {
            throw new InvalidApplicationException("Expected different amount");
        }

        result = register.makeChange(new Money(1.30), new Money(20.50));
        showAmount("MakeChange of 1,30 from 20 euro", result);

        profit = register.processEndOfDay();

        showProfit(profit);

        if (!profit.equals(new Money(0))) {
            throw new InvalidApplicationException("Expected different amount on second day");
        }
	}

	/**
	 * Show the current change amount
	 * @param message the message
	 * @param change the change
	 */
	private static void showAmount(String message, Map<Money, Integer> change) {
		if (change != null) {
			System.out.print(message);
			System.out.print(": ");
			for (Money coin : change.keySet()) { // print out "3x E 0,04 etc"
				System.out.print(change.get(coin));
				System.out.print("x ");
				System.out.print(coin);
				System.out.print("; ");
			}
			System.out.println();
		} else {
			System.out.println("Could not make change");
		}		
	}

	/**
	 * Show the profit of the day
	 * @param profit the profit
	 */
	private static void showProfit(Money profit) {
		System.out.print("Profit: ");
		System.out.println(profit.toString());
	}
}
