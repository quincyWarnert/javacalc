/**
 * Educom
 */
package nu.educom.calculateChange;

import java.util.Map;

/**
 * Create a cachRegister drawer that keeps track of all notes and coins in it's drawer 
 */
public class CashRegister {

	/**
	 * Constructor of CashRegister 
	 * 
	 * @param initialContent the initial content of the drawer
	 */
	public CashRegister(Map<Money, Integer> initialContent) {
		// TODO implement
	}

	/** 
	 * Calculate the change of the the amount to pay and return a list of returned notes 
	 * and coins and their amount
	 * For example 
	 *   MakeChange(4.51, 10) => { { 5.0, 1 }, { 0.2, 2 }, { 0.05, 1 }, { 0.01, 4 } }
	 *   meaning: one "note of 5 euro", two "coins of 20 cent", one "coin of 5 cent" and four "coins of 0.01 cent".
	 *   
     * @param toPay amount to pay
	 * @param paid the amount paid
	 * @return a list of all notes and coins that make up the change or 
     *         <code>NULL</code> if failed to make change from the cash register
	 */
	public Map<Money, Integer> makeChange(Money toPay, Money paid) {
		
           
            
		return null;
	}

	/**
	 * Calculate the total cash added to the register since creation of since last call to processEndOfDay.
	 * @return the total amount added
	 */
	public Money processEndOfDay() {
		// TODO implement
		return new Money(0);
	}
        
        private CashRegister (){
        
        }

}
